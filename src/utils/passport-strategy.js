const {privateHMAC} = require('config');
const {User} = require('../models/index.model');

// login and jwt dependencies
const passportJWT = require('passport-jwt');

const extractJwt = passportJWT.ExtractJwt;
const JwtStrategy = passportJWT.Strategy;

let jwtOptions = {};
jwtOptions.jwtFromRequest = extractJwt.fromAuthHeader();
jwtOptions.secretOrKey = privateHMAC;

/*
 * JSON Web Token (JWT) is an open standard (RFC 7519) that defines a compact
 * and self-contained way for securely transmitting information between parties
 * as a JSON object. This information can be verified and trusted because it is
 * digitally signed. JWTs can be signed using a secret (with HMAC algorithm) or
 * a public/private key pair using RSA.
 */
const strategy = new JwtStrategy(jwtOptions, function(jwtPayload, next) {
  User.findById(jwtPayload.id).then((empresa) => {
    if (empresa) {
      next(null, empresa);
    } else {
      next(null, false);
    }
  });
});

module.exports = strategy;
