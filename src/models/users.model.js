/* eslint new-cap: ["error", { "capIsNew": false }] */
const bcrypt = require('bcrypt');

module.exports = function(sequelize, DataTypes) {
  const User = sequelize.define('User', {
    id: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      autoIncrement: true,
    },
    username: {
      type: DataTypes.STRING(16),
      allowNull: false,
      validate: {
        len: {
          args: [4, 16],
          msg: 'username must be between 4 and 16 characters',
        },
        notEmpty: {msg: 'username must have a value'},
      },
    },
    email: {
      type: DataTypes.STRING(255),
      allowNull: false,
      validate: {
        isEmail: {msg: 'invalid email format'},
        notEmpty: {msg: 'email must have a value'},
      },
    },
    password: {
      type: DataTypes.STRING(255),
      allowNull: false,
      validate: {
        notEmpty: {msg: 'password must have a value'},
      },
    },
    bank_username: {
      type: DataTypes.STRING(100),
      allowNull: false,
      validate: {
        notEmpty: {msg: 'bank username must have a value'},
      },
    },
    bank_password: {
      type: DataTypes.STRING(100),
      allowNull: false,
      validate: {
        notEmpty: {msg: 'bank password must have a value'},
      },
    },
    created: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
    },
    updated: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    deleted: {
      type: DataTypes.DATE,
      allowNull: true,
    },
  }, {
    tableName: 'users',
    paranoid: true,
    createdAt: 'created',
    updatedAt: 'updated',
    deletedAt: 'deleted',
    setterMethods: {
      password(pass) {
        const salt = bcrypt.genSaltSync(8);
        this.setDataValue('password', bcrypt.hashSync(pass, salt));
      },
      bank_password(pass) {
        const salt = bcrypt.genSaltSync(8);
        this.setDataValue('bank_password', bcrypt.hashSync(pass, salt));
      },
    },
  });

  User.toResponse = (values) => {
    delete values.password;
    delete values.bank_password;
    delete values.id;
    delete values.deleted;

    return values;
  };

  return User;
};
