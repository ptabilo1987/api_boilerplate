const express = require('express');
const User = require('../controllers/users.controller');
const userRouter = require('./user.router');

const router = express.Router(); // eslint-disable-line new-cap

// this is over /api referenced in server.js
router.get('/', (req, res) => res.status(200).send({
  message: 'Welcome to the takemymymoney API!',
}));

router.post('/login', User.login);
router.post('/signup', User.register);
router.use('/user', userRouter);

module.exports = router;
