const express = require('express');
const User = require('../controllers/users.controller');
const router = express.Router(); // eslint-disable-line new-cap
const passport = require('passport');

// internal routing /movimiento/
router.get('/',
  passport.authenticate('jwt', {session: false}),
  User.currentUser);

router.put('/',
  passport.authenticate('jwt', {session: false}),
  User.updateCurrentUser);

module.exports = router;
