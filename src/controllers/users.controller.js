const {User} = require('../models/index.model');
const jwt = require('jsonwebtoken');
const {privateHMAC} = require('config');
const loginErrorMessage = 'user or password invalid';
const bcrypt = require('bcrypt');
const passportJWT = require('passport-jwt');

const user = {
  login(req, res) {
    if (!req.body.email || !req.body.password) {
      res.status(400).json({message: 'empty data'});
      return;
    }

    const email = req.body.email;
    const password = req.body.password;

    User.findOne({
      where: {email: email},
      include: [{all: true}],
    }).then((user) => {
      if (!user) {
        res.status(400).json({message: loginErrorMessage});
      }

      // Load hash from password DB.
      bcrypt.compare(password, user.password).then(function(concurr) {
        if (concurr) {
          // from now on we'll identify the user by the id and the id is
          // the only personalized value that goes into our token
          const payload = {id: user.id};
          const token = jwt.sign(payload, privateHMAC, {
            expiresIn: '1h',
          });
          user.dataValues.token = token;
          res.status(200).json({user: User.toResponse(user.dataValues)});
        } else {
          res.status(400).json({error: loginErrorMessage});
        }
      });
    });
  },
  register(req, res) {
    User
      .build({
        email: req.body.email,
        password: req.body.password,
        bank_username: req.body.bank_username,
        bank_password: req.body.bank_password,
        username: req.body.username,
      })
      .save()
      .then((user) => {
        res.status(200).json({user: User.toResponse(user.dataValues)});
      })
      .catch((error) => {
        res.status(400).json({error: error.errors});
      });
  },
  currentUser(req, res) {
    const user = req.user.dataValues;
    user.token = req.headers.authorization.split(' ')[1];
    res.status(200).json({user: User.toResponse(user)});
  },
  updateCurrentUser(req, res) {
    const user = req.user;
    user
      .update(req.body.user)
      .then((updatedUser) => {
        updatedUser.dataValues.token = passportJWT.fromAuthHeader(req);
        res.status(200).json({user: User.toResponse(updatedUser.dataValues)});
      })
      .catch((error) => {
        res.status(400).json({error: error.errors});
      });
  },
};

module.exports = user;
