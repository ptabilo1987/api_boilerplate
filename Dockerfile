FROM alpine:3.4

ARG app_env
ENV APP_ENV $app_env

# File Author / Maintainer
LABEL authors="Pato <pato@chipax.com>"

# Update & install required packages
RUN apk add --update nodejs bash git

# Install app dependencies
COPY package.json /www/package.json
RUN cd /www; npm install

# Copy app source
COPY . /www

# Set work directory to /www
WORKDIR /www

ENV PORT 3000
EXPOSE  3000

# start command as per package.json
CMD if [ ${APP_ENV} = production ]; \
	then \
	npm start; \
	else \
  npm run dev; \
  fi
