const express = require('express');
const routes = require('./src/routes/index.route');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const passport = require('passport');
const strategy = require('./src/utils/passport-strategy');
// comandos para crear modelos
// yarn global add sequelize-auto
// sudo sequelize-auto -o "./src/models/" -d facturacion -h localhost -u fact -p 3306 -x 12345 -e mysql -t empresas

/*
 * config file depend on NODE_ENV variable
 * to create your custom file create development.json on config folder
 * remember to set your NODE_ENV to development "export NODE_ENV=development"
*/
const config = require('config');

passport.use(strategy);

const app = express();
app.use(passport.initialize());
app.use(passport.session());

app.use(cors());

// logger
app.use(morgan('dev'));

// parse body params and attache them to req.body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// mount all routes on /api path
app.use('/', routes);
// catch 404 and forward to error handler
app.use((req, res) => {
  res.status(404).send({url: `${req.originalUrl} not found`});
});

app.listen(config.port, '0.0.0.0', () => {
  console.log(`Server listening on: http://localhost: ${config.port}`);
});

module.exports = app;
